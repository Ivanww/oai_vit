import torch
import torchvision
from torch import optim
from torch import nn

from catalyst.dl import SupervisedRunner
from catalyst.dl import CriterionCallback
from catalyst.dl import AccuracyCallback
from catalyst import utils

from dataset import load_data_with_label
from TransResNet import trans_resnet34
from tqdm import tqdm

def naive_train(train, val):
    model = torchvision.models.resnet34(pretrained=True)
    model.fc = nn.Linear(512, 5, bias=True)
    nn.init.normal_(model.fc.weight, 0, 0.01)
    model = model.cuda()

    learning_rate = 1e-4
    weight_decay = 1e-8
    optimizer = optim.Adam(model.parameters(), lr=learning_rate,
        weight_decay=weight_decay)

    loss_fn = nn.CrossEntropyLoss()

    num_epoch = 4
    batch_size=32

    for epoch in range(num_epoch):
        loss_tot = 0
        metrics_tot = 0
        model.train()
        for batch in tqdm(train):
            inputs = batch['image']
            targets = batch['label']

            inputs = inputs.cuda()
            inputs.requires_grad_(True)
            targets = targets.cuda()

            optimizer.zero_grad()
            out = model(inputs)
            loss = loss_fn(out, targets)

            loss.backward()
            optimizer.step()

            # m = metrics(out.detach(), targets)

            loss_tot += loss.detach() * batch_size
            # metrics_tot += m * batch_size
        print(f'Training Epoch {epoch+1}, Avg Loss {loss_tot / (len(train)*batch_size):.6f}')

import timm
def train_on_loader(train, val, logdir):
    loaders = {'train': train, 'valid': val}
    num_epochs = 16

    # model = trans_resnet34()
    model = timm.create_model('vit_base_resnet50_224_in21k', pretrained=True)
    model.head = nn.Linear(768, 5, bias=True)
    # model = torchvision.models.resnet50(pretrained=True)
    # model.head = nn.Linear(1024, 5, bias=True)
    nn.init.normal_(model.head.weight, 0, 0.01)

    learning_rate = 1e-4
    weight_decay = 1e-8
    optimizer = optim.Adam(model.parameters(), lr=learning_rate,
        weight_decay=weight_decay)

    device = utils.get_device()

    runner = SupervisedRunner(device=device, input_key='image',
        input_target_key='label')

    criterion = {
        'ce': nn.CrossEntropyLoss()
    }

    callbacks = [
        CriterionCallback(
            input_key='label',
            criterion_key='ce'
        ),
        AccuracyCallback(input_key='label', num_classes=5, topk_args=(1,))
    ]

    runner.train(
        model = model,
        criterion=criterion,
        optimizer=optimizer,
        loaders=loaders,
        callbacks=callbacks,
        logdir=logdir,
        num_epochs=num_epochs,
        main_metric='accuracy01',
        minimize_metric=False,
        fp16=None,
        verbose=True
    )

import pandas as pd
from sklearn.model_selection import train_test_split
def main_train():
    meta = '~/workspace/oai_vis/OAImeta/valid_cls_labels.pkl'
    meta = pd.read_pickle(meta)
    train, val = train_test_split(meta, test_size=0.2, random_state=337)
    root = './oai_data'
    train = load_data_with_label(train, root, batch_size=16, train=True)
    val = load_data_with_label(val, root, batch_size=32, train=False)
    train_on_loader(train, val, 'logs/resnet50')
    # naive_train(train, val)

if __name__ == '__main__':
    main_train()