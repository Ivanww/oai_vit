import torch
import torchvision
import numpy as np
import pandas as pd
import sklearn.metrics as M
from PIL import Image, ImageOps
import shutil, time, os, json
from torch.utils.data import DataLoader, Dataset
import torch.nn as nn
import os
import cv2

VALID_CLS_LABELS = 'OAImeta/valid_cls_labels.pkl'
EARLY_CLS_LABELS = 'OAImeta/early_cls_labels.pkl'
ROI_ROOT = '../OAIroi_299'
ROI_S_ROOT = './OAIroi_224'

class OAIDataset(Dataset):
    def __init__(self, meta, root, label_map=None, transform=None):
        super(OAIDataset, self).__init__()
        self.dataframe = meta
        self.root = root
        self.transform = transform
        self.label_map = label_map


    def __len__(self):
        return self.dataframe.shape[0]

    def __getitem__(self, i):
        label = self.dataframe.iloc[i, -1] # 0-4
        if self.label_map is not None:
            label = self.label_map[label]
        img = Image.open(os.path.join(
            self.root, self.dataframe.iloc[i, 0]+'.png'))
        # img = cv2.imread(os.path.join(
        #    self.root, self.dataframe.iloc[i, 0]+'.png'))

        if self.transform is not None:
            img = self.transform(img)

        return img, label

class OAIEarlyDataset(OAIDataset):
    def __init__(self, meta, root, labels = [0, 1], label_map=None, transform=None):
        meta = meta.loc[meta.V00XRKL.isin(labels), :].copy()
        super(OAIEarlyDataset, self).__init__(meta, root, label_map, transform)

class OAITestDataset(Dataset):
    def __init__(self, meta, root, label_map=None, transform=None):
        super(OAITestDataset, self).__init__()
        self.dataframe = meta
        self.root = root
        self.transform = transform
        self.label_map = label_map
    def __len__(self):
        return self.dataframe.shape[0]

    def __getitem__(self, i):
        label = self.dataframe.iloc[i, -1]
        if self.label_map is not None:
            label = self.label_map[label]
        pid = self.dataframe.iloc[i, 0]
        img = Image.open(os.path.join(
            self.root, self.dataframe.iloc[i, 0]+'.png'))
        # img = cv2.imread(os.path.join(
        #    self.root, self.dataframe.iloc[i, 0]+'.png'))

        if self.transform is not None:
            transformed = self.transform(img)

        return transformed, {'raw': img, 'label': label, 'id': pid}

class OAITestEarlyDataset(OAITestDataset):
    def __init__(self, meta, root, labels=[0, 1], label_map=None, transform=None):
        meta = meta.loc[meta.V00XRKL.isin(labels), :].copy()
        super(OAITestEarlyDataset, self).__init__(meta, root, label_map, transform)

def load_data_with_label(df, root, batch_size=32, train=False, mean=0.4305, std=0.2024, labels=None, label_map=None):
    if train:
        transform = torchvision.transforms.Compose([
            torchvision.transforms.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0.1),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize([mean]*3, [std]*3)
        ])
    else:
        transform = torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize([mean]*3, [std]*3)
        ])

    if labels is None:
        ds = OAIDataset(df, root, label_map, transform)
    else:
        ds = OAIEarlyDataset(df, root, labels, label_map, transform)
    loader = DataLoader(ds, batch_size=batch_size, shuffle=train)
    return loader

def test_collate_fn(batch):
    images = torch.stack([b[0] for b in batch])
    raw_images = [b[1]['raw'] for b in batch]
    labels =  torch.tensor([b[1]['label'] for b in batch]).long()
    ids = [b[1]['id'] for b in batch]
    return images, {'label': labels, 'id': ids, 'raw': raw_images}

def load_data_with_name(df, root, batch_size=32, mean=0.4305, std=0.2024, labels=None, label_map=None):
    if labels is None:
        ds = OAITestDataset(df, root, label_map,
            torchvision.transforms.Compose([
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize([mean]*3, [std]*3)
            ]))
    else:
        ds = OAITestEarlyDataset(df, root, labels, label_map,
            torchvision.transforms.Compose([
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize([mean]*3, [std]*3)
            ]))

    loader = DataLoader(ds, batch_size=batch_size, shuffle=False, collate_fn=test_collate_fn)
    return loader

# MSE 0.9 - 1

if __name__ == '__main__':
    df = pd.read_pickle(VALID_CLS_LABELS)
    train = load_data_with_label(df, ROI_S_ROOT, train=False, labels=[0, 1])
    for imgs, labels in train:
        print(imgs.shape)
        print(labels)
        break