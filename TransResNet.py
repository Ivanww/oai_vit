import torch
import torchvision
import torch.nn as nn
from torchvision.models import ResNet
from torchvision.models.resnet import _resnet
from torchvision.models.resnet import model_urls
from torchvision.models.utils import load_state_dict_from_url
from torchvision.models.resnet import BasicBlock, Bottleneck
from typing import Type, Any, Callable, Union, List, Optional

from vit_pytorch.vit_pytorch import Transformer

class TransResNet(ResNet):
    # args inherited from ResNet
    def __init__(self, block, layers, num_classes=1000, 
        zero_init_residual=False, groups=1, width_per_group=64, replace_stride_with_dilation=None, norm_layer=None):
        super().__init__(block, layers, num_classes=num_classes,
            zero_init_residual=zero_init_residual, groups=groups, width_per_group=width_per_group,
            replace_stride_with_dilation=replace_stride_with_dilation, norm_layer=norm_layer)

        in_dim = 512 * block.expansion # output channels of layer 4
        emb_dim = in_dim
        # OPTION: compress the in_dim via 
        # emb_dim = 64 # number of feature used for each patch
        # self.to_embedding = nn.Conv2d(in_dim, emb_dim, 1)
        
        # Transformer Parameters
        num_block = 1
        num_head = 1
        head_dim = 64
        mlp_dim = 64
        self.transformer = Transformer(emb_dim, num_block, num_head, head_dim, mlp_dim, 0.1)
    

    def _forward_impl(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        # call transformer here
        # OPTIONAL: compress channel
        # x = self.to_embedding(x)
        batch_size, c, h, w = x.shape
        x = x.flatten(2).permute(0, 2, 1).contiguous()
        x = self.transformer(x)
        x = x.permute(0, 2, 1).contiguous().view(batch_size, c, h, w)

        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)

        return x

def _trans_resnet(
    arch: str,
    block: Type[Union[BasicBlock, Bottleneck]],
    layers: List[int],
    pretrained: bool,
    progress: bool,
    **kwargs: Any):

    # set num_classes = 5 for OA
    model = TransResNet(block, layers, num_classes=5, **kwargs)

    if pretrained:
        state_dict = load_state_dict_from_url(model_urls[arch],
                                              progress=progress)
        # remove weights of last FC
        state_dict.pop('fc.weight')
        state_dict.pop('fc.bias')

        try:
            keys = model.load_state_dict(state_dict, strict=False)
        except Exception:
            print('Error loading weights')
            pass
        finally:
            # missing, unexpected = keys
            # assert no "unexpected keys"
            assert len(keys[1]) == 0
            # only last linear layer and transformer's weights are not loaded
            num_trans_params = len(model.transformer.state_dict())
            assert num_trans_params + 2 == len(keys[0])
            print('Skip loading weights to transformer')
    
    return model


def trans_resnet34():
    return _trans_resnet('resnet34', BasicBlock, [3, 4, 6, 3], True, True)

def trans_resnet50():
    return _trans_resnet('resnet50', Bottleneck, [3, 4, 6, 3], True, True)

if __name__ == '__main__':
    # model = trans_resnet34()
    model = trans_resnet50()
    x = torch.randn(1, 3, 224, 224)
    out = model(x)

    print(out.shape)