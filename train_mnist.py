import time
import torch
import torchvision
import torch.optim as optim
import torch.nn.functional as F

from vit_pytorch import ViT

# 1. Python Env Management - conda, pip
#   conda activate vit -- switch the python env
#   pip install -e . -- install via symbol link
# 2. Python Libaraies
#   [numpy] -
#   [scipy] - machine learning algorithm, metrics etc. <accuracy>
#   [torch] - 1. GPU version numpy
#           - 2. Deep learning modules - Layers, Optimizers, Function "sigmoid, relu" => Auto Grad
#   [torchvision] - helper modules for data loading, pre-trained CNN etc.
#   [PIL, openCV] - read/write images
#   [matplotlib] - draw figures

# 3. Deep learning procedures
#   1. build / initialize model
#   2. load and augment data
#   3. define loss function
#   4. optimizer
#   5. evaluate model performance : torch -> numpy
#   6. save results -> torch -> save_module
#   7. debugging training process

# m - conv1
#   |- bn1
#   |- relu
#   |- Sequential -|

torch.manual_seed(42)

# 2 -> load MNIST dataset
DOWNLOAD_PATH = './data/mnist'
BATCH_SIZE_TRAIN = 100
BATCH_SIZE_TEST = 1000

# Tensor -> torch's array
# numpy -> ndarray
transform_mnist = torchvision.transforms.Compose([torchvision.transforms.ToTensor(),
                               torchvision.transforms.Normalize((0.1307,), (0.3081,))])

train_set = torchvision.datasets.MNIST(DOWNLOAD_PATH, train=True, download=True,
                                       transform=transform_mnist)
train_loader = torch.utils.data.DataLoader(train_set, batch_size=BATCH_SIZE_TRAIN, shuffle=True)

test_set = torchvision.datasets.MNIST(DOWNLOAD_PATH, train=False, download=True,
                                      transform=transform_mnist)
test_loader = torch.utils.data.DataLoader(test_set, batch_size=BATCH_SIZE_TEST, shuffle=True)

def train_epoch(model, optimizer, data_loader, loss_history):
    total_samples = len(data_loader.dataset)
    model.train()

    for i, (data, target) in enumerate(data_loader):
        data = data.cuda()
        target = target.cuda()
        optimizer.zero_grad()
        output = F.log_softmax(model(data), dim=1)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()

        if i % 100 == 0:
            print('[' +  '{:5}'.format(i * len(data)) + '/' + '{:5}'.format(total_samples) +
                  ' (' + '{:3.0f}'.format(100 * i / len(data_loader)) + '%)]  Loss: ' +
                  '{:6.4f}'.format(loss.item()))
            loss_history.append(loss.item())

def evaluate(model, data_loader, loss_history, best_acc=None):
    model.eval()

    total_samples = len(data_loader.dataset)
    correct_samples = 0
    total_loss = 0

    with torch.no_grad():
        for data, target in data_loader:
            data = data.cuda()
            target = target.cuda()
            output = F.log_softmax(model(data), dim=1)
            loss = F.nll_loss(output, target, reduction='sum')
            _, pred = torch.max(output, dim=1)

            total_loss += loss.item()
            correct_samples += pred.eq(target).sum()

    avg_loss = total_loss / total_samples
    loss_history.append(avg_loss)
    print('\nAverage test loss: ' + '{:.4f}'.format(avg_loss) +
          '  Accuracy:' + '{:5}'.format(correct_samples) + '/' +
          '{:5}'.format(total_samples) + ' (' +
          '{:4.2f}'.format(100.0 * correct_samples / total_samples) + '%)\n')
    acc = correct_samples / total_samples

    if best_acc is None or acc > best_acc:
        torch.save({'state_dict': model.state_dict()},
                'best_model.pth')
        best_acc = acc
    return best_acc

N_EPOCHS = 25

start_time = time.time()
model = ViT(image_size=28, patch_size=7, num_classes=10, channels=1,
            dim=64, depth=6, heads=8, mlp_dim=128)
model = model.cuda() # model.to('cpu')
optimizer = optim.Adam(model.parameters(), lr=0.003) # SGD(model.parameter(), )

best_acc = None
train_loss_history, test_loss_history = [], []
for epoch in range(1, N_EPOCHS + 1):
    print('Epoch:', epoch)
    train_epoch(model, optimizer, train_loader, train_loss_history)
    best_acc = evaluate(model, test_loader, test_loss_history, best_acc)

print('Execution time:', '{:5.2f}'.format(time.time() - start_time), 'seconds')
